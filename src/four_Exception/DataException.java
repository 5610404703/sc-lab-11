package four_Exception;

public class DataException extends Exception {
	public DataException(){
		super();
	}

	public DataException(String string) {
		super(string);
	}

}
