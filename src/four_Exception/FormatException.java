package four_Exception;

public class FormatException extends RuntimeException {
	public FormatException(){
		super();
	}
	
	public FormatException(String string){
		super(string);
	}
}
