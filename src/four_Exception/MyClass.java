package four_Exception;

public class MyClass {
	
	public void methX() throws DataException{//5
		//code
		throw new DataException();		
	}
	
	public void methY(){//6
		//code
		throw new FormatException();		
	}
	
	public static void main(String[] args) {
		try { MyClass c = new MyClass();
			System.out.println("A");
			c.methX();
			System.out.println("B");
			c.methY();
			System.out.println("C");
			return;
			
		} catch (DataException e){
			System.out.println("D");
		} catch (FormatException e){
			System.out.println("E");
		} finally {
			System.out.println("F");
		}
		
		System.out.println("G");

	}
}
//		1.����� new object DataException() �������� declare ����� throws DataException ���� pass control �  method methX() 
//		���ͧ�ҡ  DataException �� Checked Exception ��ͧ�ա�� declare �������� new object FormatException() 
//      2.A 
//		  B
//		  C
//		  F
//		  G
//		3.A 
//		  D
//		  B
//		  C
// 		  F
// 		  G
//		4.A 
//		  B
//		  E
// 		  F
// 		  G
