package three_HashMap;
import java.util.HashMap;


public class WordCounter {
	private String message;
	HashMap<String,Integer> wordCount;
	
	public WordCounter(String message){
		this.message = message;
		wordCount = new HashMap<String,Integer>();		
	}
	
	public void count(){
		String[] wordSplit = message.split(" ");
		for(String word: wordSplit){
			if(wordCount.containsKey(word)){
				wordCount.put(word, wordCount.get(word)+1);
			}else{
				wordCount.put(word, 1);
			}
		}
	}
	
	public int hasWord(String word){
		if(wordCount.containsKey(word)){
			return wordCount.get(word);
		}
		return 0;	
	 }
	 
	 public static void main(String[] args) throws InterruptedException {
		 WordCounter counter = new WordCounter("here is the root of the root and the bud of the bud");
		 counter.count();
		 System.out.println(counter.hasWord("root"));
		 System.out.println(counter.hasWord("leaf"));
	 }
}
