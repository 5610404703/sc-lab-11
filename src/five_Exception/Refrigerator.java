package five_Exception;
import java.util.ArrayList;


public class Refrigerator {
	
	private int size;
	private ArrayList<String> things;
	
	public Refrigerator(){
		this.size = 5;
		things = new ArrayList<String>();

	}
	
	public void put(String stuff)throws FullException{
		if(things.size()<size){
			things.add(stuff);
		}
		else {
			throw new FullException("Your refrigetator are full!!");		
		}
		
	}
	
	public String takeOut(String stuff){		
		if(things.contains(stuff)){
			things.remove(stuff);
			return stuff;	
		}
		return null;
	}
	
	public String toString(){
		String lstStr = "";
		for(String t : things){
			lstStr += t+"\n";
		}
		return lstStr;
	}
	

}