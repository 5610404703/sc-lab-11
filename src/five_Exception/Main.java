package five_Exception;

public class Main {
	
	public static void main(String args[]) {
		Refrigerator ref = new Refrigerator();
		try {
			ref.put("Fish");
			ref.put("Watermelon");
			ref.put("Milk");
			ref.put("Egg");
			ref.put("Apple");
			System.out.println("***STUFF IN REFRIGERATOR***\n"+ref.toString());
			ref.put("Checken");
			
		} catch (FullException e) {
			System.err.println("Error : "+e.getMessage());
		}
	}

}
