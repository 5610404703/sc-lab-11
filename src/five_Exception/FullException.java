package five_Exception;

public class FullException extends Exception{
	
	public FullException(){
		super();
	}
	
	public FullException(String str){
		super(str);
	}
}
